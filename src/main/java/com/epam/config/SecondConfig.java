package com.epam.config;

import com.epam.model.bean3.BeanE;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

@Configuration
@ComponentScan("com.epam.model.bean2")
@ComponentScan(basePackages = "com.epam.model.bean3",
        includeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = BeanE.class))
public class SecondConfig {
}
