package com.epam.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan("com.epam.model.bean1")
@ComponentScan("com.epam.model.bean4")
//@Import(SecondConfig.class)
public class FirstConfig {

}
