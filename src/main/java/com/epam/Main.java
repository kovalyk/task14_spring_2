package com.epam;

import com.epam.config.FirstConfig;
import com.epam.model.bean1.General;
import com.epam.model.bean4.Config;
import com.epam.model.bean4.GeneralBean;
import com.epam.model.bean4.OtherBeanA;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(FirstConfig.class);
//context.getBeanDefinitionNames();
context.getBean(GeneralBean.class).getBean();
context.getBean(General.class).printBean();
    }
}
