package com.epam.model.bean1;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("beanValues.properties")
@Qualifier("myBean")
public class BeanA implements Bean {
    @Value("${beanA.id}")
    private int id;
    @Value("${beanA.name}")
    private String name;

    public BeanA(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
   public String getBean(){
        return "The id is " + id + " The name is " + name;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "BeanA{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
