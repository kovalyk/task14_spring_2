package com.epam.model.bean1;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("beanValues.properties")
public class BeanC implements Bean {
    @Value("${beanC.id}")
    private int id;
    @Value("${beanC.name}")
    private String name;

    public BeanC(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String getBean(){
       return "The id is " + id + " The name is " + name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "BeanC{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
