package com.epam.model.bean1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class General {
    @Autowired
    @Qualifier("myBean")
    List<Bean> beans;

    public void printBean(){
        for (Bean bean : beans) {
            System.out.println(bean.getBean());
        }
    }
}
