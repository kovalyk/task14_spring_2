package com.epam.model.bean2;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("beanValues.properties")
public class RoseFlower {
    @Value("${roseFlower.id}")
    private int id;
    @Value("${roseFlower.name}")
    private String name;

    public RoseFlower(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "RoseFlower{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}

