package com.epam.model.bean2;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("beanValues.properties")
public class CatAnimal {
    @Value("${catAnimal.id}")
    private int id;
    @Value("${catAnimal.name}")
    private String name;

    public CatAnimal(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "CatAnimal{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
