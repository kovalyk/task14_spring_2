package com.epam.model.bean2;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("beanValues.properties")
public class NarcissusFlower {
    @Value("${narcissusFlower.id}")
    private int id;
    @Value("${narcissusFlower.name}")
    private String name;

    public NarcissusFlower(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "NarcissusFlower{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
