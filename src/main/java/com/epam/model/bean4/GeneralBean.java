package com.epam.model.bean4;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class GeneralBean {
@Autowired
   private List<Bean> beans;

public void getBean(){
    for (Bean bean : beans) {
        System.out.println(bean.getBean());
    }
}
}
