package com.epam.model.bean4;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(2)
public class OtherBeanA implements Bean {

    @Override
    public String getBean() {
        return "The name is OtherBeanA";
    }

}
