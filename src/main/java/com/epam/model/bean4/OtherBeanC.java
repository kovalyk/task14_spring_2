package com.epam.model.bean4;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class OtherBeanC implements Bean {

    @Override
    public String getBean() {
        return "The name is OtherBeanC";
    }
}
