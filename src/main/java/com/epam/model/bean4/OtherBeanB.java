package com.epam.model.bean4;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(3)
public class OtherBeanB implements Bean {

    @Override
    public String getBean() {
        return "The name is OtherBeanB";
    }
}
